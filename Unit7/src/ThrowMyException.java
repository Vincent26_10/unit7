
public class ThrowMyException {
	public static void main(String[] args) {
		divide(1, 2);
	}

	public static void divide(int i, int j) {
		if (i < j) {
			throw new ArithmeticException();
		} else {
			System.out.println(i / j);
		}

	}
}
