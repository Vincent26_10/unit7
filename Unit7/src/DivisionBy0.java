
public class DivisionBy0 {

	public static void main(String[] args) {
		method();
	}

	public static void method() {
		try {
			System.out.println("A");
			int x = 12 / 0;
			System.out.println("B");
		} catch (ArithmeticException ex) {
			System.out.println("0");
		}
	}

}
