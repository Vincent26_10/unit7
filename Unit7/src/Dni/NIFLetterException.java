package Dni;

public class NIFLetterException extends NIFException {
	public NIFLetterException() {
		super("Nif_exception");
	}
	
	public NIFLetterException(String msg) {
		super(msg);
	}
}
