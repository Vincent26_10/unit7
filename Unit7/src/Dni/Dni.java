package Dni;

public class Dni {
	
	public static final char[] LETTERS = {'T','R','W','A',
			'G','M','Y','F','P','D','X','B','N','J','Z',
			'S','Q','V','H','L','C','K','E'}; 
	
	private int number;
	private char letter;

	public Dni() {
		number = 0;
		letter = 'X';
	}
	
	public Dni(int number, char letter) throws NIFLetterException {
		String s = "" + letter;
		s = s.toUpperCase();		
		this.letter = s.charAt(0);
		char newLeter = LETTERS[number % 23];
		if (newLeter != this.letter) {
			throw new NIFLetterException("Incorrect letter for that number");
		} else {
			this.number = number;
		}
	}

	public Dni(int number) throws NIFException {
		if (number<0) {
			throw new NIFException("Invalid negative number");
		}
		if (number > 99999999) {
			throw new NIFException("Invalid number. It has more than 8 digits");
		}
		this.number = number;
		letter = LETTERS[number % 23];
	}
	
	public Dni(String s) {
		letter = s.charAt(s.length()-1);
		String sNumbers = s.replaceAll("[^\\d]", "");
		number = Integer.parseInt(sNumbers);
		char rightLetter = LETTERS[number % 23];
		if (rightLetter != letter) {
			number = -number;
		}		
	}
	
	public int getNumber() {
		return number;
	}
	
	public char getLetter() {
		return letter;
	}
	
	public void setNumber(int number) {
		this.number = number;
		char rightLetter = LETTERS[number % 23];
		if (rightLetter != letter) {
			number = -number;
		}
	}
	@Override
	public String toString() {
		return ""+number+letter;
	}
	
	public String toFormattedString() {
		StringBuilder stBuilder = new StringBuilder();
		String sNumber = String.valueOf(number);
		stBuilder.append(sNumber);
		if (number > 999) {
			stBuilder.insert(sNumber.length()-3, ".");
		}
		if (number > 999999) {
			stBuilder.insert(sNumber.length()-6, ".");
		}
		if (number > 999999999) {
			stBuilder.insert(sNumber.length()-9, ".");
		}
		stBuilder.append("-"+letter);
		return stBuilder.toString();
	}
	
	public boolean correctDni() {		
		if (number<0) {
			return false;
		} else {
			return true;
		} 
	}
	
	public static char letterForDni(int number) {
		return LETTERS[number % 23];
	}
	
	public static String NifForDni(int number) throws NIFException {
		Dni d = new Dni(number);
		return d.toFormattedString();
	}
	
	
}
