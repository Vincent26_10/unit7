package Dni;

public class NIFException extends Exception {

	public NIFException() {
		super("Nif_exception");
	}
	
	public NIFException(String msg) {
		super(msg);
	}
}
