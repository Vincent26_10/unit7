import java.util.*;
import java.io.*;
public class TestCatch3 {

	public static void main(String[] args) {
		try {
		method("myFile.txt");
		}catch (FileNotFoundException ex) {
			System.out.println("File doesn't exist");
		}
	}

	private static void method(String string)  throws FileNotFoundException{
		Scanner in;
		File f = new File(string);
		in = new Scanner(f);
	}

}
